# FakeCompany-Frontend
## Install
```
git@gitlab.com:cldershem/webpack-tester.git
cd webpack-tester
npm install
```

## Run
```
npm run dev
```

## Build
```
npm run build
```
