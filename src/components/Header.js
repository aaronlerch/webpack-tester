import React, { Component } from 'react';

import Logo from '../assets/img/logo.jpg';

class NavItem extends Component {
  render() {
    return (
      <a href={this.props.url}>{this.props.label}</a>
    )
  }
}


export default class Header extends Component {
  render() {
    const items = [
      { label: 'overview', url: '#' },
      { label: 'blog', url: '#' },
      { label: 'signup', url: '#' },
    ];

    return (
      <header>
        <nav>
          <img className="logo" src={Logo} alt="Logo" />
          <div className="nav-buttons">
            {items.map(item =>
              <NavItem key={item.label} url={item.url} label={item.label} />
            )}
          </div>
        </nav>
      </header>
   );
  }
}
